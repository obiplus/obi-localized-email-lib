require('regenerator-runtime/runtime');
const i18next = require('i18next');
const Backend = require('i18next-chained-backend');
const LocalBackend = require('i18next-node-fs-backend');
const RemoteBackend = require('i18next-node-remote-backend');
const sgMail = require('@sendgrid/mail');
const Handlebars = require('handlebars');
const fs = require('fs');

const main = (i18nOptions, sendGridOptions, templatePath) => new Promise(async (res, rej) => {
  const i18nOpts = Object.assign({
    fallbackLng: 'en',
    load: 'all',
    preload: ['en'],
    ns: ['default'],
    defaultNS: 'default',
    debug: false,
    saveMissing: false,
    returnObjects: true
  }, i18nOptions);

  const primaryPath = i18nOptions.primaryPath ? i18nOptions.primaryPath : '';
  const secondaryPath = i18nOptions.secondaryPath ? i18nOptions.secondaryPath : '';

  // Init i18next
  try{
    await new Promise((resolve, reject) => {
      i18next
        .use(Backend)
        .init(Object.assign({
          backend: {
            backends: [
              RemoteBackend.default, // primary
              LocalBackend // fallback
            ],
            backendOptions: [{
              loadPath: primaryPath,
              addPath: null,
              allowMultiLoading: false
            }, {
            // path where resources get loaded from
              loadPath: secondaryPath,
              addPath: null
            }]
          }
        }, i18nOpts), (err) => {
          if (err) return reject(err);
          return resolve(i18next);
        });
    });
  }
  catch(err){
    return rej(err);
  }

  // init email client
  sgMail.setApiKey(sendGridOptions.token);
  const se = (to, subject, html, attachments) => new Promise(async (resolve, reject) => {
    const email = {
      to,
      from: sendGridOptions.from,
      subject,
      html,
      attachments: attachments ? attachments : []
    };
    try {
      await sgMail.sendMultiple(email);
      resolve();
    } catch (error) {
      reject(error);
    }
  });

  const sendEmail = (to, templateId, locale, params, attachments = null) => new Promise(async (resolve, reject) => {
    await new Promise(loaded => i18next.loadLanguages([locale], (err) => {
      if (err) console.log(`[Translation] Missing language one of: ${locale}`);
      return loaded();
    }));
    const t = i18next.getFixedT(locale, templateId);

    // Apply
    const translatedParameters = {};
    Object.keys(params).forEach((v) => { translatedParameters[v] = t(params[v]); });

    try{
      const reportHtml = fs.readFileSync(`${templatePath}/${templateId}.html`).toString();
      const template = Handlebars.compile(reportHtml);
      await se(to, translatedParameters.subject, template(translatedParameters), attachments);
      resolve(null);
    }
    catch(err){
      reject(err);
    }
  });

  return res(sendEmail);
});

module.exports = main;
